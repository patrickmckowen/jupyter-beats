import os
import pathlib 
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.io import wavfile
import librosa, librosa.display
import IPython.display as ipd
import sounddevice as sd
import soundfile as sf

from ipywidgets import interact, interactive, fixed
import ipywidgets as widgets
from IPython.display import display

    
#package_dir = pathlib.Path(parent_dir_to_python_path('../../../../'))
data_dir = pathlib.Path('data')


#import scanner.files.audio.stanford_mir
#import scanner.files.audio.thinkdsp.thinkdsp as thinkdsp
#import scanner.files.audio.thinkdsp.thinkplot as thinkplot
#import scanner.files.audio.thinkdsp.thinkstats2 as thinkstats2
